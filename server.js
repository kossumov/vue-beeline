const express = require('express')
var cors = require('cors')
const app = express()
const bodyParser = require("body-parser");
const port = 3000
var fs = require('fs');

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var obj = {
    quotes: []
}

//REST API 
app.get('/', (req, res) => res.send('Hello World!'))

app.get('/list', (req, res) =>  {
    console.log("reading...");
    fs.readFile('quotes.json', (err, data) => {
        console.log("read");
        if (err) throw err;
        var jsson = JSON.parse(data)
        console.log(jsson);
        res.send(JSON.stringify(jsson))
    });
    console.log("done");
})
app.post('/list', function (req, res) {
    console.log("writing...");
    var quote = req.body.quote;
    console.log(JSON.stringify(quote, null, 4))
    writeJSONData(JSON.stringify(quote, null, 4));

    // obj.quotes.push(quote);
    // var jsonData = JSON.stringify(obj, null, 4);
    // writeJSONData(jsonData);
    // res.send('ok');
  });


function writeJSONData(data) {
    fs.writeFile('quotes.json', data, (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    });

}

app.listen(port, () => console.log(`Example app listening on port ${port}!`))